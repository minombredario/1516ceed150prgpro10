/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.modelo;

import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.PooledConnection;
import org.ceedcv.ceed150prgpro10.controlador.HibernateUtil;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author usu1601
 */

public class ModeloHibernate implements IModelo{
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    private String basedatos = "citamedica";
    private String DireccionBaseDatos = "jdbc:mysql://localhost/" + basedatos;
    private String user = "alumno";
    private String pass = "alumno";
    //conexion a la base de datos
    private Connection conexion = null;
    private Statement st;
    private String error;
    private VistaTerminal vt;
    private int id = 0;
    //pool de conexciones
    //private MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    //private PooledConnection pool = null;
    
    public ModeloHibernate(){
        /*mcpds.setUser(user);
        mcpds.setPassword(pass);
        mcpds.setUrl(DireccionBaseDatos);*/
        
    }

    @Override
    public void create(Pacientes paciente) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(paciente);
            session.getTransaction().commit();
            vt.info("Paciente " + paciente.getNombre() + " creado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void update(Pacientes paciente) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(paciente);
            session.getTransaction().commit();
            vt.info("Alumno " + paciente.getNombre() + " actualizado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void delete(Pacientes paciente) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(paciente);
            session.getTransaction().commit();
            vt.info("Paciente " + paciente.getNombre() + " borrado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
            vt.error("El paciente no se ha podido borrar. Consultar si tiene citas activas, y eliminar primero la cita. \n"
                    + "Error: " + he.getMessage());
        }
    }

    @Override
    public ArrayList<Pacientes> readp() {
        ArrayList pacientes = new ArrayList();
		try {
                    String hql = "from Pacientes";
                    Session session = HibernateUtil.getSessionFactory().openSession();
                    session.beginTransaction();
                    Query q = session.createQuery(hql);
                    pacientes = (ArrayList) q.list();
                    session.getTransaction().commit();
                } catch (HibernateException he) {
                    he.printStackTrace();
                }
                return pacientes;
    }

    @Override
    public void create(Medicos medico) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(medico);
            session.getTransaction().commit();
            vt.info("Medico " + medico.getNombre() + " creado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void update(Medicos medico) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(medico);
            session.getTransaction().commit();
            vt.info("Medico " + medico.getNombre() + " actualizado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void delete(Medicos medico) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(medico);
            session.getTransaction().commit();
            vt.info("Medico " + medico.getNombre() + " borrado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
            vt.error("El medico no se ha podido borrar. Consultar si tiene citas activas, y eliminar primero la cita. \n"
                    + "Error: " + he.getMessage());
        }
    }

    @Override
    public ArrayList<Medicos> readm() {
        ArrayList medicos = new ArrayList();
		try {
                    String hql = "from Medicos";
                    Session session = HibernateUtil.getSessionFactory().openSession();
                    session.beginTransaction();
                    Query q = session.createQuery(hql);
                    medicos = (ArrayList) q.list();
                    session.getTransaction().commit();
                } catch (HibernateException he) {
                    he.printStackTrace();
                }
                return medicos;
    }

    @Override
    public void create(Citas cita) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(cita);
            session.getTransaction().commit();
            vt.info("La cita del paciente " + cita.getPacientes().getNombre() + " creada correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void update(Citas cita) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(cita);
            session.getTransaction().commit();
            vt.info("La cita del paciente " + cita.getPacientes().getNombre() + " actualizada correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    @Override
    public void delete(Citas cita) {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(cita);
            session.getTransaction().commit();
            vt.info("La cita del paciente " + cita.getPacientes().getNombre() + " borrado correctamente");
        } catch (HibernateException he) {
            he.printStackTrace();
            vt.error("La cita no se ha podido borrar.\n" + "Comprobar medicos o pacientes activos en ella \n"
                    + "Error: " + he.getMessage());
        }
    }

    @Override
    public ArrayList<Citas> readc() {
        ArrayList citas = new ArrayList();
		try {
                    String hql = "from Citas";
                    Session session = HibernateUtil.getSessionFactory().openSession();
                    session.beginTransaction();
                    Query q = session.createQuery(hql);
                    citas = (ArrayList) q.list();
                    session.getTransaction().commit();
                } catch (HibernateException he) {
                    he.printStackTrace();
                }
                return citas;
    }

    @Override
    public void instalar() {
        conexion = null;
        st = null;
        error = null;

        try {
                      
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            String jdbcUrl = "jdbc:mysql://localhost";
            conexion = DriverManager.getConnection(jdbcUrl, user, pass);

            st = conexion.createStatement();
            String sql = "CREATE DATABASE IF NOT EXISTS " + basedatos + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
           if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    error = e.getMessage();
                } // nothing we can do
            }
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                } // nothing we can do
            }
        }
        desconectar();
        
    }

    @Override
    public void desinstalar() {
        conexion = null;
		st = null;
		error = null;
		try {
			conectar();
			st = conexion.createStatement();
			String sql = "DROP DATABASE IF EXISTS " + basedatos + ";";
			System.out.println(sql);
			st.executeUpdate(sql);
		} catch (Exception e) {
			error = e.getMessage();
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					error = e.getMessage();
				}
			}
			if (conexion != null) {
				try {
					conexion.close();
				} catch (SQLException se) {
					error = se.getMessage();
				}
			}
		}
    }

    @Override
    public void conectar() {
        try{
        Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            conexion = (Connection)DriverManager.getConnection(DireccionBaseDatos, user, pass); // para conectarnos a la base de datos debe de estar creada.
            System.out.println("Conexion establecida");
        }
        catch (SQLException e){//error de sql 
            System.out.println("Error de MySQL");
            vt.info("No esta conectado con la base de datos");
        }
        catch(Exception e){//imprime cualquier otro error
           System.out.println("Se ha encontrado el siguiente error: " + e.getMessage());
           vt.info("Se ha encontrado el siguiente error: " + e.getMessage());
        }
    }

    @Override
    public void desconectar() {
        try {
            System.out.println("BDR Mysql Connexión cerrada");
            this.conexion.close();
        }catch (SQLException se){
           se.printStackTrace();
        }
    }
    public void CrearTablas(){
        
        error = null;
        String sql;
        conectar();
        try {
            st = conexion.createStatement();

            sql = "drop table if exists medicos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "drop table if exists pacientes;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "drop table if exists citas;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `medicos` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`nombre` varchar(50) NOT NULL,\n"
               + "`dni` varchar(10) NOT NULL, \n"
               + "`n_colegiado` varchar(10) NOT NULL, \n"
               + "`especialidad` varchar(30) NOT NULL, \n"
               + "`edad` int(5) NOT NULL,\n"
               + "`telefono` int(10) NOT NULL, \n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "CREATE TABLE IF NOT EXISTS `pacientes` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`nombre` varchar(50) NOT NULL,\n"
               + "`dni` varchar(10) NOT NULL, \n"
               + "`nss` varchar(15) NOT NULL, \n"
               + "`edad` int(5) NOT NULL,\n"
               + "`telefono` int(10) NOT NULL, \n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            
            sql = "CREATE TABLE IF NOT EXISTS `citas` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`dia` date NOT NULL,\n"
               + "`hora` varchar(5) NOT NULL, \n"
               + "`id_paciente` int(5) NOT NULL, \n"
               + "`id_medico` int(5) NOT NULL,\n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`),\n"
               + "FOREIGN KEY (`id_paciente`)REFERENCES pacientes(id),\n"
               + "FOREIGN KEY (`id_medico`)REFERENCES medicos(id)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectar();
        
    }
    
    
    public void CrearDatos() {
        error = null;
        String sql;
        conectar();
        try {
 
            
            st = conexion.createStatement();

            sql = "insert into medicos (id,nombre,dni,n_colegiado,especialidad,edad,telefono,observaciones) "
                + "values(1,'Dario Navarro Andrés','53359063Y','12/1234/12','cabecera','34','646136523','observaciones');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into pacientes (id,nombre,dni,nss,edad,telefono,observaciones ) "
                + "values(1,'Dario Navarro Andrés','53359063Y','12/1234567/12','34','646136523','obbservaciones');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "insert into citas (id,dia,hora,id_paciente,id_medico,observaciones ) "
                + "values(1,'2016/03/12','12:30',1,1,'observaciones');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectar();
        
    }
}
