package org.ceedcv.ceed150prgpro10.modelo;

/**
 * Fichero: persona.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 14-dic-2015
 */
public class Persona {
    
    protected String nombre;
    protected String dni;
    protected int edad;
    protected int telefono;
    protected String observaciones;
    
    public Persona(){
  
   }

   public Persona (String nombre, String dni, int edad, int telefono,
            String observaciones){
     this.nombre = nombre;
     this.dni = dni;
     this.edad = edad;
     this.telefono = telefono;
     this.observaciones = observaciones;
     
    }
   
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setDni(String dni){
        this.dni = dni;
    }
    
    public String getDni(){
        return dni;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String toString() {
        return nombre + edad + telefono +observaciones;
    }

}