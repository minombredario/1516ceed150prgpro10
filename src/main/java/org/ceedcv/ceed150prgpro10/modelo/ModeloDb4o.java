/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;

/**
 *
 * @author usu1601
 */
public class ModeloDb4o implements IModelo{
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private int idp = 1;
    private int idm = 1;
    private int idc = 1;
    private VistaTerminal vt;
    private static String file = "bdoo.db4o";
    private File filebd = new File(file);
    private ObjectContainer objectcontainer;
    private ObjectSet resultado = null;

    public File getFilebd() {
        return filebd;
    }

    public ModeloDb4o() throws IOException{
        if (filebd.exists()){
            this.idp = contPaciente();
            this.idm = contMedico();
            this.idc = contCita();
        }else{
            vt.advertencia("No hay BBDD, \ndebes de instalarla");
        }
    }
  
    public void conectar(){
        
        objectcontainer = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), file);
        System.out.println("Conectado");
    }
  
    public void desconectar(){
        
        objectcontainer.close();
        System.out.println("desconectado");
    }
  
    public void instalar(){
        try{
        Pacientes paciente1 = new Pacientes(1,"Dario Navarro Andrés","53359063Y",34,641652338,"soy paciente 1","12/1234567/12");
        Pacientes paciente2 = new Pacientes(2,"Juan Perico","53359063Y",43,646136523,"soy paciente 2","34/56789876/34");
            
        Medicos medico1 = new Medicos(1,"Andres medico","53359063Y",56,646136523,"soy medico 1","12/1234/12","cabecera");
        Medicos medico2 = new Medicos(2,"Palomo Cojo","53359063Y",25,646136523,"soy medico 2","12/1234/12","cabecera");
            
        create(paciente1);
        create(paciente2);
        create(medico1);
        create(medico2);
            
        Citas cita1 = new Citas(1, sdf.parse("2016-03-12"), "12:30", "soy cita 1", medico2, paciente1);
        Citas cita2 = new Citas(2, sdf.parse("2016-03-26"), "13:45", "soy cita 2", medico1, paciente2);
            
        create(cita1);
        create(cita2);
            
        idp = 3;
        idm = 3;
        idc = 3;
        
        vt.info("Datos creados.");
        
        } catch (ParseException pe) {
            pe.printStackTrace();
            vt.error("Ha ocurrido un error durante la instalación."
                    + "Error: " + pe.getMessage());
        }
    }   
    
  
    public void desinstalar(){
        try {
            idp = 1;
            idm = 1;
            idc = 1;
            filebd.delete();
            vt.info("Desinstalación completada");
        } catch (Exception e) {
            e.printStackTrace();
            vt.error("Error: " + e.getMessage() +"\ndurante la desinstalación.");
        }
    }
     @Override
    public void create(Pacientes paciente) {
       
        conectar();
        paciente.setId(idp);
        idp++;
        objectcontainer.store(paciente);
        desconectar();
        System.out.println("Paciente " + paciente.getNombre() + " creado correctamente.");
        
    }

    @Override
    public void update(Pacientes paciente) {
        Pacientes panull = new Pacientes(paciente.getId());
        resultado = null;
        
        conectar();  
        resultado = objectcontainer.queryByExample(panull);//igualamos la consulta al paciente null
        if (resultado.hasNext()) {//mientras halla resultados vaya añadiendolos 
            Pacientes pa = (Pacientes)resultado.next();
            pa.setNombre(paciente.getNombre());
            pa.setDni(paciente.getDni());
            pa.setNss(paciente.getNss());
            pa.setObservaciones(paciente.getObservaciones());
            pa.setEdad(paciente.getEdad());
            pa.setTelefono(paciente.getTelefono());
            objectcontainer.store(pa);
            System.out.println("Paciente " + pa.getNombre() + " actualizado correctamente.");
            vt.info("Paciente " + pa.getNombre() + " actualizado correctamente.");
        }
        desconectar();
    }

    @Override
    public void delete(Pacientes paciente) {
        Pacientes panull = new Pacientes(paciente.getId());
        resultado = null;
        
        conectar();  
        resultado = objectcontainer.queryByExample(panull);
            if (resultado.hasNext()) {
                Pacientes pa = (Pacientes)resultado.next();
                objectcontainer.delete(pa);
                System.out.println("Paciente " + pa.getNombre() + "eliminado correctamente.");
                vt.info("Paciente " + pa.getNombre() + " borrado correctamente.");
            }
        desconectar();
    }

    @Override
    public ArrayList<Pacientes> readp() {
        
        ArrayList pacientes = new ArrayList();
        Pacientes panull = new Pacientes(0, null, null, 0, 0, null, null);
        
        conectar();
        resultado = this.objectcontainer.queryByExample(panull);
        while (resultado.hasNext()) {
            Pacientes paciente = (Pacientes)resultado.next();
            pacientes.add(paciente);  
        }
        desconectar();
        return pacientes;
    }

    @Override
    public void create(Medicos medico) {
        conectar();
        medico.setId(idm);
        idm++;
        objectcontainer.store(medico);
        desconectar();
        System.out.println("Medico " + medico.getNombre() + " creado correctamente.");
    }

    @Override
    public void update(Medicos medico) {
       
        Medicos docnull = new Medicos(medico.getId());
        resultado = null;
        conectar();  
        resultado = objectcontainer.queryByExample(docnull);
        
        if (resultado.hasNext()) {
            Medicos doc = (Medicos)resultado.next();
            
            doc.setNombre(medico.getNombre());
            doc.setDni(medico.getDni());
            doc.setEspecialidad(medico.getEspecialidad());
            doc.setNColegiado(medico.getNColegiado());
            doc.setObservaciones(medico.getObservaciones());
            doc.setEdad(medico.getEdad());
            doc.setTelefono(medico.getTelefono());
            
            objectcontainer.store(doc);
            System.out.println("Medico " + doc.getNombre() + " actualizado correctamente.");
            vt.info("Medico " + doc.getNombre() + " actualizado correctamente.");
        }
        desconectar();
    }

    @Override
    public void delete(Medicos medico) {
        
        Medicos docnull = new Medicos(medico.getId());
        resultado = null;
        
        conectar();  
        resultado = objectcontainer.queryByExample(docnull);
            if (resultado.hasNext()) {
                Medicos doc = (Medicos)resultado.next();
                objectcontainer.delete(doc);
                System.out.println("Medico " + doc.getNombre() + "eliminado correctamente.");
                vt.info("Medico " + doc.getNombre() + " borrado correctamente.");
            }
        desconectar();
    }

    @Override
    public ArrayList<Medicos> readm() {
        ArrayList medicos = new ArrayList();
        Medicos docnull = new Medicos(0, null, null, 0, 0, null, null, null);
        resultado = null;
        
        conectar();
        resultado = this.objectcontainer.queryByExample(docnull);
        while (resultado.hasNext()) {
            Medicos medico = (Medicos)resultado.next();
            medicos.add(medico);  
        }
        desconectar();
        return medicos;
    }

    @Override
    public void create(Citas cita) {
        //idc = contCita();
        conectar();
        cita.setId(idc);
        //para evitar la duplicidad de los objetos, los buscamos y solo los agrega si no existes
        Pacientes paciente = objPaciente(cita);
        Medicos medico = objMedico(cita);
            if(paciente != null){
                cita.setPacientes(paciente);
            }
            if(medico != null){
                cita.setMedicos(medico);
            }
        idc++;
        objectcontainer.store(cita);
        desconectar();
        System.out.println("La cita del paciente " + cita.getPacientes().getNombre() + " creada correctamente.");
        
    }

    @Override
    public void update(Citas cita) {
        
        Citas cinull = new Citas(cita.getId());
        resultado = null;
        conectar();  
        resultado = objectcontainer.queryByExample(cinull);
        
        if (resultado.hasNext()) {
            
            Citas ci = (Citas)resultado.next();
            Pacientes paciente = objPaciente(cita);
            Medicos medico = objMedico(cita);
           
            ci.setDia(cita.getDia());
                
            ci.setHora(cita.getHora());
            if(paciente != null){
                ci.setPacientes(paciente);
            }
            if(medico != null){
                ci.setMedicos(medico);
            }
            //ci.setMedico(cita.getMedico());
            //ci.setPaciente(cita.getPaciente());
            ci.setObservaciones(cita.getObservaciones());
            
            objectcontainer.store(ci);
            System.out.println("La cita del paciente " + cita.getPacientes().getNombre() + " actualizada correctamente.");
            vt.info("La cita del paciente " + cita.getPacientes().getNombre() + " actualizada correctamente.");
        }
        desconectar();
    }

    @Override
    public void delete(Citas cita) {
        Citas cinull = new Citas(cita.getId());
        resultado = null;
        
        conectar();  
        resultado = objectcontainer.queryByExample(cinull);
            if (resultado.hasNext()) {
                Citas ci = (Citas)resultado.next();
                objectcontainer.delete(ci);
                 System.out.println("La cita del paciente " + cita.getPacientes().getNombre() + " actualizada correctamente.");
            vt.info("La cita del paciente " + cita.getPacientes().getNombre() + " actualizada correctamente.");
            }
        desconectar();
    }

    @Override
    public ArrayList<Citas> readc() {
        ArrayList citas = new ArrayList();
        Citas cinull = new Citas(0, null, null, null, null, null);
        resultado = null;
        
        conectar();
        resultado = this.objectcontainer.queryByExample(cinull);
        while (resultado.hasNext()) {
            Citas cita = (Citas)resultado.next();
            citas.add(cita);  
        }
        desconectar();
        return citas;
    }
    
    

//****************************BUSCADORES DE OBJETOS*******************************//
    
    
    public Pacientes getPaciente(Pacientes paciente) {
        
        Pacientes pa = new Pacientes(paciente.getId());
        
        conectar();
        resultado = this.objectcontainer.queryByExample(pa);
        while (resultado.hasNext()) {
            paciente = (Pacientes)resultado.next();
        }
        desconectar();
        return paciente;   
        
    }
    
    public Medicos getMedico(Medicos medico) {
        
        Medicos doc = new Medicos(medico.getId());
        
        conectar();
        resultado = this.objectcontainer.queryByExample(doc);
        while (resultado.hasNext()) {
            medico = (Medicos)resultado.next();
        }
        desconectar();
        return medico;   
        
    }
    
    public Citas getCita(Citas cita) {
        
        Citas ci = new Citas(cita.getId());
        
        conectar();
        resultado = this.objectcontainer.queryByExample(ci);
        while (resultado.hasNext()) {
            cita = (Citas)resultado.next();
        }
        desconectar();
        return cita;   
        
    }
    
    public Pacientes objPaciente(Citas cita){
        
        Pacientes paciente = null;
        Pacientes pa = new Pacientes(cita.getPacientes().getId());
        
        resultado = this.objectcontainer.queryByExample(pa);
        paciente = (Pacientes)resultado.next();
        return paciente;
    }
    
   public Medicos objMedico(Citas cita){
        
        Medicos medico = null;
        Medicos doc = new Medicos(cita.getMedicos().getId());
        
        resultado = this.objectcontainer.queryByExample(doc);
        medico = (Medicos)resultado.next();
        return medico;
    }
    
//*************************CALCULADORES DE ID MAXIMA****************************//
    private int contPaciente(){
        int idmayor = 0;
        Pacientes panull = new Pacientes(0, null, null, 0, 0, null, null);
        Pacientes paciente = null;
        conectar();
        resultado = this.objectcontainer.queryByExample(panull);
        while (resultado.hasNext()) {
            paciente = (Pacientes)resultado.next();
            int id = paciente.getId();
            if(id > idmayor){
                idmayor = id;
            }
        }
        desconectar();
        return idmayor + 1;
    }
    private int contMedico(){
        int idmayor = 0;
        Medicos docnull = new Medicos(0, null, null, 0, 0, null, null, null);
        Medicos medico = null;
        conectar();
        resultado = this.objectcontainer.queryByExample(docnull);
        while (resultado.hasNext()) {
            medico = (Medicos)resultado.next();
            int id = medico.getId();
                if(id > idmayor){
                    idmayor = id;
                }
        }
        desconectar();
        return idmayor + 1;
    } 
   private int contCita(){
        int idmayor = 0;
        Citas cinull = new Citas(0, null, null, null, null, null);
        Citas cita = null;
        conectar();
        resultado = this.objectcontainer.queryByExample(cinull);
        while (resultado.hasNext()) {
            cita = (Citas)resultado.next();
            int id = cita.getId();
                if(id > idmayor){
                    idmayor = id;
                }
        }
        desconectar();
        return idmayor + 1;
    } 

    @Override
    public void CrearTablas() {
        //no hace nada
    }

    @Override
    public void CrearDatos() {
        //no hace nada
    }
    
    
 
   
    
}
