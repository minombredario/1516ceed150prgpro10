/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;



/**
 *
 * @author usu1601
 */
public  class ModeloFichero implements IModelo{
    
    private File filepacientes = new File("pacientes.csv");
    private File filemedicos = new File("medicos.csv");
    private File filecitas = new File("citas.csv");
    private int idcountpa;
    private int idcountme;
    private int idcountci;
    private Date dia;
    private VistaTerminal vt = new VistaTerminal();
    
    private ArrayList pacientes;
    private ArrayList medicos;
    private ArrayList citas;
    
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    public void create(Pacientes paciente) {
        idcountpa = contPaciente()+1;
        paciente.setId(idcountpa);
        try {
            FileWriter filewriter = new FileWriter(filepacientes, true);
            filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getDni()  + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
            filewriter.close();
           
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }

    @Override
    public void update(Pacientes paciente) {
        File temp_pa = new File("temp.csv");
        Pacientes pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Pacientes(id, nombre, dni, edad, telefono,
              observaciones, nss);
                
                if (paciente.getId() == (id)) {
                    filewriter.write(
                      paciente.getId()   + ";" + paciente.getNombre() + ";"
                    + paciente.getDni()  + ";"
                    + paciente.getNss()  + ";" + paciente.getObservaciones() + ";"
                    + paciente.getEdad() + ";" + paciente.getTelefono() +";\r\n");
                } else {
                    filewriter.write(
                      pa.getId()   + ";" + pa.getNombre() + ";"
                    + pa.getDni()  + ";"
                    + pa.getNss()  + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad() + ";" + pa.getTelefono() +";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public void delete(Pacientes paciente) {
        File temp_pa = new File("temp.csv");
        Pacientes pa;
        try {
            FileWriter filewriter = new FileWriter(temp_pa, true);
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                pa = new Pacientes(id, nombre, dni, edad, telefono,
              observaciones, nss);
                if (paciente.getId() != id) {
                    filewriter.write(
                      pa.getId()        + ";" + pa.getNombre()        + ";"
                    + paciente.getDni() + ";"
                    + pa.getNss()       + ";" + pa.getObservaciones() + ";"
                    + pa.getEdad()      + ";" + pa.getTelefono()      +";\r\n");
                } 
                s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filepacientes.delete();
        boolean rename = temp_pa.renameTo(new File("pacientes.csv"));
    }

    @Override
    public ArrayList<Pacientes> readp() {
        
        pacientes = new ArrayList();
        Pacientes paciente = new Pacientes();
        
        if (filepacientes.exists()){
        try {
            FileReader filereader = new FileReader(filepacientes);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String nss = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                paciente = new Pacientes(id, nombre, dni, edad, telefono,
               nss, observaciones);
                pacientes.add(paciente);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
         }else {vt.mostrarTexto("El fichero no esta creado");
                vt.advertencia("El fichero no esta creado");}
        
        return pacientes;
    }

   @Override
    public void create(Medicos medico) {
       idcountme = contMedico()+1;
       medico.setId(idcountme);
        try {
            FileWriter filewriter = new FileWriter(filemedicos, true);
            
            
            filewriter.write(
                    
                      medico.getId()            + ";" + medico.getNombre()     + ";"
                    + medico.getDni()           + ";"
                    + medico.getObservaciones() + ";" + medico.getNColegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
            filewriter.close();
                
           
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void update(Medicos medico) {
        File temp_me = new File("temp.csv");
        Medicos doc;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                                
                doc = new Medicos(id, nombre, dni, edad, telefono, observaciones, 
                        ncolegiado, especialidad);
                if (medico.getId() == id) {
                    filewriter.write(
                      medico.getId()            + ";" + medico.getNombre()     + ";"
                    + medico.getDni()           + ";"
                    + medico.getObservaciones() + ";" + medico.getNColegiado() + ";"         
                    + medico.getEspecialidad()  + ";" + medico.getEdad()       + ";"
                    + medico.getTelefono()      + ";\r\n");
                    
                } else {
                    filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getDni()           + ";"
                    + doc.getObservaciones() + ";" + doc.getNColegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad()       + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public void delete(Medicos medico) {
        File temp_me = new File("temp.csv");
        Medicos doc = null;
        try {
            FileWriter filewriter = new FileWriter(temp_me, true);
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                String observaciones = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                doc = new Medicos(id, nombre, dni, edad, telefono,
                        ncolegiado, especialidad, observaciones);
                if (medico.getId() != id) {
                     filewriter.write(
                      doc.getId()            + ";" + doc.getNombre()     + ";"
                    + doc.getDni()           + ";"
                    + doc.getObservaciones() + ";" + doc.getNColegiado() + ";"         
                    + doc.getEspecialidad()  + ";" + doc.getEdad()       + ";" 
                    + doc.getTelefono()      + ";\r\n");
                }
                s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }System.out.print (doc);
        filemedicos.delete();
        boolean rename = temp_me.renameTo(new File("medicos.csv"));
    }

    @Override
    public ArrayList<Medicos> readm() {
        medicos = new ArrayList();
        Medicos medico = new Medicos();
        
        if (filemedicos.exists()){
        try {
            FileReader filereader = new FileReader(filemedicos);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                int id = Integer.parseInt(st.nextToken());
                String nombre = st.nextToken();
                String dni = st.nextToken();
                String observaciones = st.nextToken();
                String ncolegiado = st.nextToken();
                String especialidad = st.nextToken();
                int edad = Integer.parseInt(st.nextToken());
                int telefono = Integer.parseInt(st.nextToken());
                
                medico = new Medicos(id, nombre, dni, edad, telefono, observaciones,
                        ncolegiado, especialidad );
                medicos.add(medico);
                s = bufereader.readLine();
            }
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }else {vt.mostrarTexto("El fichero no esta creado");
               vt.advertencia("El fichero no esta creado");}
        return medicos;
      }

    @Override
    public void create(Citas cita) {
        
        idcountci = contCita()+1;
        cita.setId(idcountci);
        try {
            FileWriter filewriter = new FileWriter(filecitas, true);
            filewriter.write(
                      cita.getId()               + ";" + sdf.format(cita.getDia())          + ";" 
                    + cita.getHora()             + ";" + cita.getObservaciones() + ";"
                    + cita.getPacientes().getId() + ";" + cita.getMedicos().getId() + "\r\n");
            filewriter.close();
            
        } catch (IOException ex) {
            Logger.getLogger(ModeloFichero.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void update(Citas cita) {
        File temp_ci = new File("temp.csv");
        Citas ci;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
                
                Pacientes paciente = new  Pacientes (idpa);
                Medicos medico = new Medicos(idme);                                    
                ci = new Citas(id, dia, hora, observaciones, medico, paciente);
                
                if (cita.getId() != (id)) {
                     filewriter.write(
                      cita.getId()               + ";" + sdf.format(cita.getDia()) + ";" 
                    + cita.getHora()             + ";" + cita.getObservaciones()   + ";" 
                    + cita.getPacientes().getId() + ";" + cita.getMedicos().getId()  + ";" 
                    + "\r\n");
                   
                } else {
                    filewriter.write(
                      ci.getId()               + ";" + sdf.format(ci.getDia()) + ";" 
                    + ci.getHora()             + ";" + ci.getObservaciones()   + ";" 
                    + ci.getPacientes().getId() + ";" + ci.getMedicos().getId()  + ";" 
                    + "\r\n");
                }

                s = bufereader.readLine();

            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public void delete(Citas cita) {
        File temp_ci = new File("temp.csv");
        Citas ci = null;
        try {
            FileWriter filewriter = new FileWriter(temp_ci, true);
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s;
            s = bufereader.readLine();
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                 
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
               
                
                Pacientes paciente = new  Pacientes (idpa);
                Medicos medico = new Medicos(idme);
                
               
                ci = new Citas(id, dia, hora, observaciones, medico, paciente);
                if (cita.getId() != (id)) {
                     filewriter.write(
                      ci.getId()               + ";" + sdf.format(ci.getDia()) + ";" 
                    + ci.getHora()             + ";" + ci.getObservaciones()   + ";" 
                    + ci.getPacientes().getId() + ";" + ci.getMedicos().getId()  + ";" 
                    + "\r\n");
                }
                                
               s = bufereader.readLine();
            }
            filewriter.close();
            filereader.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        filecitas.delete();
        boolean rename = temp_ci.renameTo(new File("citas.csv"));
    }

    @Override
    public ArrayList<Citas> readc() {
        citas = new ArrayList();
        Citas cita = new Citas();
                
        if(filecitas.exists()){
        try {
            FileReader filereader = new FileReader(filecitas);
            BufferedReader bufereader = new BufferedReader(filereader);
            String s = bufereader.readLine();
            
            while (s != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                
                int id = Integer.parseInt(st.nextToken());
                try{
                    dia = sdf.parse(st.nextToken());
                }catch (ParseException pe){}
                String hora = st.nextToken();
                String observaciones = st.nextToken();
                int idpa = Integer.parseInt(st.nextToken());
                int idme = Integer.parseInt(st.nextToken());
                
               
                Pacientes paciente = getPaciente(idpa);
                cita.setPacientes(paciente);
                
                Medicos medico = getMedico(idme);
                cita.setMedicos(medico);
                
                cita = new Citas(id, dia, hora, observaciones, medico, paciente);
                
                citas.add(cita);
                s = bufereader.readLine();
            } 
            filereader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }else {vt.mostrarTexto("El fichero no esta creado");
           vt.advertencia("El fichero no esta creado");}
        return citas;
    }
    
     private Pacientes getPaciente(int id) {

        Pacientes paciente_cita = null;
        
        Iterator it = readp().iterator();
        while (it.hasNext()) {
            Pacientes paciente = (Pacientes) it.next();
            if (id == (paciente.getId())) {
                paciente_cita = paciente;
            }
        }
        return paciente_cita;

    }
    
    private Medicos getMedico(int id) {

        Medicos medico_cita = null;
        
        Iterator it = readm().iterator();
        while (it.hasNext()) {
            Medicos medico = (Medicos) it.next();
            if (id == (medico.getId())) {
                medico_cita = medico;
            }
        }
        return medico_cita;

    }
    
    private int contMedico(){
        int idmayor = 0;
        Medicos medico;
        
            if (this.filemedicos.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filemedicos);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        medico = new Medicos(id);
                        medico.setId(id);
                        int idmedico = medico.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idmedico){
                                idmayor = idmedico;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }
    
    
    
    private int contPaciente(){
        int idmayor = 0;
        Pacientes paciente;
        
            if (this.filepacientes.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filepacientes);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        paciente = new Pacientes(id);
                        paciente.setId(id);
                        int idpaciente = paciente.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idpaciente){
                                idmayor = idpaciente;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }
    private int contCita(){
        int idmayor = 0;
        Citas cita;
        
            if (this.filecitas.exists()) {      
                try {
           
                    FileReader filereader = new FileReader(this.filecitas);
                    BufferedReader bufereader = new BufferedReader(filereader);
                    String s;
                    s = bufereader.readLine();
                    
                    while (s != null) {
                        //extraemos el id del medico   
                        StringTokenizer st = new StringTokenizer(s, ";");

                        int id = Integer.parseInt(st.nextToken());
                        cita = new Citas(id);
                        cita.setId(id);
                        int idcita = cita.getId();
                        //si el id del medico es mayor se lo asignamos a idmayor
                            if (idmayor < idcita){
                                idmayor = idcita;
                            }s = bufereader.readLine(); 
                    }filereader.close();                   
                }catch (IOException localIOException) {}
        }
        return idmayor;
    }

    @Override
    public void instalar() {
        //No hace nada
    }

    @Override
    public void desinstalar() {
        //No hace nada    
    }

    @Override
    public void conectar() {
         //No hace nada
    }

    @Override
    public void desconectar() {
         //No hace nada
    }

    @Override
    public void CrearTablas() {
         //No hace nada
    }

    @Override
    public void CrearDatos() {
         //No hace nada
    }

   
  
}
