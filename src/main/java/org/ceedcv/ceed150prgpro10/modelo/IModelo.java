/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.modelo;

import java.util.ArrayList;


/**
 * Fichero: Imodelo.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public interface IModelo {
    
    //******PACIENTE******

    public void create(Pacientes paciente);

    public void update(Pacientes paciente);

    public void delete(Pacientes paciente);
        
    public ArrayList <Pacientes> readp();
    
   
     //******MEDICO******

    public void create(Medicos medico);

    public void update(Medicos medico);

    public void delete(Medicos medico);
    
    public ArrayList <Medicos> readm ();
    
    
    //********CITA********
    
    public void create(Citas cita);//Añade una cita al final

    public void update(Citas cita);//Actualiza los datos de la cita

    public void delete(Citas cita);//Borra el objeto seleccionado*/
       
    public ArrayList <Citas> readc ();

//********ACCESO DB4o********
    public void instalar();
  
    public void desinstalar();
    
    public void conectar();
    
    public void desconectar();
        
    public void CrearTablas();
    
    public void CrearDatos();
}
