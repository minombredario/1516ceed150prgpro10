package org.ceedcv.ceed150prgpro10.modelo;

/**
 * Fichero: Paciente.java
 *
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @date 17-oct-2015
 */
public class Paciente extends Persona {

    private String nss;
    private int id = 1;
        
    public Paciente(int id){
        this.id = id;
    }
    
    public Paciente (int id, String nombre,String dni, int edad, int telefono,
              String observaciones, String nss){
          super(nombre, dni, edad, telefono, observaciones);
          this.id = id;
          this.nss =nss;
     
    }

    public Paciente() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

      
    /**
     * @return the nss
     */
    public String getNss() {
        return nss;
    }

    /**
     * @param nss the nss to set
     */
    public void setNss(String nss) {
        this.nss = nss;
    }

    public String toString() {
        return "" + id + ": " + nombre; /*+ "\nEdad: " + edad +
                "\nTelefono: " + telefono + "\nObservaciones: " + observaciones;*/
    }

}
