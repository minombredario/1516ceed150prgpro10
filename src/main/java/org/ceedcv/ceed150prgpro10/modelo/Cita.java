package org.ceedcv.ceed150prgpro10.modelo;

import java.util.Date;

/**
 * Fichero: Cita.java
 *
 * @author Dario Navarro Andres <minombredario@gmail.com>
 * @date 13-oct-2015
 */
public class Cita {

    private int id;
    private Date dia;
    private String hora;
    private Paciente paciente;
    private Medico medico;
    private String observaciones;
   
    public Cita(){
        id = 0;
        dia = null;
        hora = "";
        medico = null;
        paciente = null;
        observaciones = null;
        
    }
    public Cita(int id){
        this.id = id;
    }
    
    public Cita(int id, Date dia, String hora, String observaciones, Medico medico, Paciente paciente){
        this.id = id;
        this.dia = dia;
        this.hora = hora;
        this.observaciones = observaciones;
        this.paciente = paciente;
        this.medico = medico;
        
    }

   
    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }
    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }
    
    public String toString(){
        return "" + hora;
    }

}
