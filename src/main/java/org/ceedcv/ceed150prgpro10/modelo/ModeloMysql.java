/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro10.modelo;

import java.sql.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
//import javax.sql.PooledConnection;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;

/**

* Fichero: ModeloMysql.java

* @author Darío Navarro Andrés <minombredario@gmail.com>

* @date 27-mar-2016

*/
public class ModeloMysql implements IModelo{
    
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    
    private String basedatos = "consultamedica";
    private String DireccionBaseDatos = "jdbc:mysql://localhost/" + basedatos;
    private String user = "alumno";
    private String pass = "alumno";
    //conexion a la base de datos
    private Connection conexion = null;
    private Statement st;
    private String error;
    private VistaTerminal vt;
    private int id = 0;
    //pool de conexciones
    //private MysqlConnectionPoolDataSource mcpds = new MysqlConnectionPoolDataSource();
    //private PooledConnection pool = null;
    
    
    public ModeloMysql(){
        /*mcpds.setUser(user);
        mcpds.setPassword(pass);
        mcpds.setUrl(DireccionBaseDatos);*/
        
    }
    
    public void conectar() {
         try{
        Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            conexion = (Connection)DriverManager.getConnection(DireccionBaseDatos, user, pass); // para conectarnos a la base de datos debe de estar creada.
            System.out.println("Conexion establecida");
        }
        catch (SQLException e){//error de sql 
            System.out.println("Error de MySQL");
            vt.info("No esta conectado con la base de datos");
        }
        catch(Exception e){//imprime cualquier otro error
           System.out.println("Se ha encontrado el siguiente error: " + e.getMessage());
           vt.info("Se ha encontrado el siguiente error: " + e.getMessage());
        }
            
    }
    
    @Override
    public void instalar() {
        conexion = null;
        st = null;
        error = null;

        try {
                      
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            String jdbcUrl = "jdbc:mysql://localhost";
            conexion = DriverManager.getConnection(jdbcUrl, user, pass);

            st = conexion.createStatement();
            String sql = "CREATE DATABASE IF NOT EXISTS " + basedatos + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
           if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    error = e.getMessage();
                } // nothing we can do
            }
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (SQLException se) {
                    error = se.getMessage();
                } // nothing we can do
            }
        }
        desconectar();
        
    }

    @Override
    public void desinstalar() {
        conexion = null;
		st = null;
		error = null;
		try {
			conectar();
			st = conexion.createStatement();
			String sql = "DROP DATABASE IF EXISTS " + basedatos + ";";
			System.out.println(sql);
			st.executeUpdate(sql);
		} catch (Exception e) {
			error = e.getMessage();
		} finally {
			if (st != null) {
				try {
					st.close();
				} catch (SQLException e) {
					error = e.getMessage();
				}
			}
			if (conexion != null) {
				try {
					conexion.close();
				} catch (SQLException se) {
					error = se.getMessage();
				}
			}
		}
    }
    
    public void desconectar(){
        try {
            System.out.println("BDR Mysql Connexión cerrada");
            this.conexion.close();
        }catch (SQLException se){
           se.printStackTrace();
        }
    } 
    
    public void CrearTablas(){
        
        error = null;
        String sql;
        conectar();
        try {
            st = conexion.createStatement();

            sql = "drop table if exists medicos;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "drop table if exists pacientes;";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "drop table if exists citas;";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS `medicos` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`nombre` varchar(50) NOT NULL,\n"
               + "`dni` varchar(10) NOT NULL, \n"
               + "`n_colegiado` varchar(10) NOT NULL, \n"
               + "`especialidad` varchar(30) NOT NULL, \n"
               + "`edad` int(5) NOT NULL,\n"
               + "`telefono` int(10) NOT NULL, \n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "CREATE TABLE IF NOT EXISTS `pacientes` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`nombre` varchar(50) NOT NULL,\n"
               + "`dni` varchar(10) NOT NULL, \n"
               + "`nss` varchar(15) NOT NULL, \n"
               + "`edad` int(5) NOT NULL,\n"
               + "`telefono` int(10) NOT NULL, \n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            
            sql = "CREATE TABLE IF NOT EXISTS `citas` (\n"
               + "`id` int(5) NOT NULL AUTO_INCREMENT,\n"
               + "`dia` date NOT NULL,\n"
               + "`hora` varchar(5) NOT NULL, \n"
               + "`id_paciente` int(5) NOT NULL, \n"
               + "`id_medico` int(5) NOT NULL,\n"
               + "`observaciones` varchar(250), \n"                    
               + "PRIMARY KEY (`id`),\n"
               + "FOREIGN KEY (`id_paciente`)REFERENCES pacientes(id),\n"
               + "FOREIGN KEY (`id_medico`)REFERENCES medicos(id)\n"
               + ") ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0;\n";
            
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectar();
        
    }
    
    
    public void CrearDatos() {
        error = null;
        String sql;
        conectar();
        try {
            st = conexion.createStatement();

            sql = "insert into medicos (id,nombre,dni,n_colegiado,especialidad,edad,telefono,observaciones) "
                + "values(1,'Dario Navarro Andrés','53359063Y','12/1234/12','cabecera','34','646136523','medico 1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "insert into medicos (id,nombre,dni,n_colegiado,especialidad,edad,telefono,observaciones) "
                + "values(2,'Pepito Grillo','53359063Y','12/1234/12','tocador','45','696439987','medico 2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            sql = "insert into pacientes (id,nombre,dni,nss,edad,telefono,observaciones ) "
                + "values(1,'Dario Navarro Andrés','53359063Y','12/1234567/12','34','646136523','paciente 1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "insert into pacientes (id,nombre,dni,nss,edad,telefono,observaciones ) "
                + "values(2,'Paquito Chocolatero','53359063Y','12/1234567/12','34','646136523','paciente 2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "insert into citas (id,dia,hora,id_paciente,id_medico,observaciones ) "
                + "values(1,'2016/03/12','12:30',2,1,'cita 1');\n";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            sql = "insert into citas (id,dia,hora,id_paciente,id_medico,observaciones ) "
                + "values(2,'2016/05/22','18:30',1,2,'cita 2');\n";
            System.out.println(sql);
            st.executeUpdate(sql);

            st.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(ModeloMysql.class.getName()).log(Level.SEVERE, null, ex);
        }
        desconectar();
        
    }

    @Override   
    public void create(Pacientes paciente){
        try{
          conectar();
          this.st = this.conexion.createStatement();
  
      
        //int id                  = paciente.getId();
        String nombre           = paciente.getNombre();
        String dni              = paciente.getDni();
        String nss              = paciente.getNss();
        int edad                = paciente.getEdad();
        int telefono            = paciente.getTelefono();
        String observaciones    = paciente.getObservaciones();
        
        String sql = "insert into pacientes(nombre,dni,nss,edad,telefono,observaciones) "
                   + "values('" + nombre + "','" + dni + "','" + nss + "','" + edad + "','" + telefono + "','" + observaciones + "')";

        System.out.println(sql);
        int salida = this.st.executeUpdate(sql);

        sql = "select max(id) as idmayor from pacientes;";
        System.out.println(sql);
        ResultSet resultado = this.st.executeQuery(sql);
        if (resultado.next()) {
            this.id = resultado.getInt("idmayor");
        }
        
        paciente.setId(this.id);
        desconectar();
          
        }catch (SQLException se){
          se.printStackTrace();
        }
      }
    @Override
    public void update(Pacientes paciente) {
        try {
            conectar();
            st = this.conexion.createStatement();
            String sql = "UPDATE pacientes  "
                                 
            + "  SET nombre='"      + paciente.getNombre()
            + "', dni='"            + paciente.getDni()
            + "', nss='"            + paciente.getNss()
            + "', edad='"           + paciente.getEdad()
            + "', telefono='"       + paciente.getTelefono()
            + "', observaciones='"  + paciente.getObservaciones()
            + "' WHERE id="         + paciente.getId()
            + ";";
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            desconectar();
            vt.info("Paciente " + paciente.getNombre() + " actualizado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    @Override
    public void delete(Pacientes paciente) {
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "DELETE FROM pacientes"
                +  " WHERE id=" + paciente.getId() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            desconectar();
            vt.info("Paciente " + paciente.getNombre() + " borrado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
            vt.error("El paciente no se ha podido borrar. Consultar si tiene citas activas, y eliminar primero la cita. \n"
                    + "Error: " + se.getMessage());
        }
    }
    
    @Override
    public ArrayList readp() {
        ArrayList pacientes = null;
        
            try {
            pacientes = new ArrayList();
            String tabla = "pacientes";
                conectar();
                st = (Statement) conexion.createStatement();
                String sql = "SELECT * \n"
                    + "FROM pacientes\n"
                    + "ORDER BY id;";
                System.out.println(sql);
                ResultSet resultado = st.executeQuery(sql);

                while (resultado.next()) {
                    Pacientes paciente = new Pacientes();

                    int id = resultado.getInt("id");
                    String nombre = resultado.getString("nombre");
                    String dni = resultado.getString("dni");
                    String nss = resultado.getString("nss");
                    int edad = resultado.getInt("edad");
                    int telefono = resultado.getInt("telefono");
                    String observaciones = resultado.getString("observaciones");


                    paciente.setId(id);
                    paciente.setNombre(nombre);
                    paciente.setDni(dni);
                    paciente.setNss(nss);
                    paciente.setEdad(edad);
                    paciente.setTelefono(telefono);
                    paciente.setObservaciones(observaciones);

                    pacientes.add(paciente);
                }
            } catch (SQLException se) {
                se.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            desconectar();
        return pacientes;
    }
   
        @Override   
        public void create(Medicos medico){
            try{
              conectar();
              this.st = this.conexion.createStatement();

            //int id                  = paciente.getId();
            String nombre           = medico.getNombre();
            String dni              = medico.getDni();
            String n_colegiado      = medico.getNColegiado();
            String especialidad     = medico.getEspecialidad();
            int edad                = medico.getEdad();
            int telefono            = medico.getTelefono();
            String observaciones    = medico.getObservaciones();

            String sql = "insert into medicos(nombre,dni ,n_colegiado,especialidad,edad,telefono,observaciones) values('" + nombre + "','" + dni + "','" + n_colegiado + "','" + especialidad + "','" + edad + "','" + telefono + "','" + observaciones + "')";

            System.out.println(sql);
            int salida = this.st.executeUpdate(sql);

            sql = "select max(id) as idmayor from medicos;";
            System.out.println(sql);
            ResultSet resultado = this.st.executeQuery(sql);
            if (resultado.next()) {
                this.id = resultado.getInt("idmayor");
            }

            medico.setId(this.id);
            desconectar();

            }catch (SQLException se){
              se.printStackTrace();
            }
            
      }
    @Override
    public void update(Medicos medico) {
        try {
            conectar();
            st = this.conexion.createStatement();
            String sql = "UPDATE medicos  "
            + "  SET nombre='"      + medico.getNombre()
            + "',dni='"             + medico.getDni()
            + "',n_colegiado='"     + medico.getNColegiado()
            + "',especialidad='"    + medico.getEspecialidad()
            + "',edad='"            + medico.getEdad()
            + "',telefono='"        + medico.getTelefono()
            + "',observaciones='"   + medico.getObservaciones()
            + "' WHERE id="         + medico.getId()
            + ";";
            
            System.out.println(sql);
            st.executeUpdate(sql);
            
            desconectar();
            vt.info("El medico " + medico.getNombre() + ", se ha actualizado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    @Override
    public void delete(Medicos medico) {
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "DELETE FROM medicos  "
                +  " WHERE id=" + medico.getId() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            desconectar();
            vt.info("El medico " + medico.getNombre() + ", se ha borrado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
            vt.error("El medico no se ha podido borrar. Consultar si tiene citas activas, y eliminar primero la cita. \n"
                    + "Error: " + se.getMessage());
        }
    }
   
    @Override
    public ArrayList readm() {
        ArrayList medicos = new ArrayList();
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "SELECT * \n"
                + "FROM medicos\n"
                + "ORDER BY id;";
            System.out.println(sql);
            ResultSet resultado = st.executeQuery(sql);

            while (resultado.next()) {
                Medicos medico = new Medicos();
        
                int id                  = resultado.getInt("id");
                String nombre           = resultado.getString("nombre");
                String dni              = resultado.getString("dni");
                String n_colegiado      = resultado.getString("n_colegiado");
                String especialidad     = resultado.getString("especialidad");
                int edad                = resultado.getInt("edad");
                int telefono            = resultado.getInt("telefono");
                String observaciones    = resultado.getString("observaciones");
                

                medico.setId(id);
                medico.setNombre(nombre);
                medico.setDni(dni);
                medico.setNColegiado(n_colegiado);
                medico.setEspecialidad(especialidad);
                medico.setEdad(edad);
                medico.setTelefono(telefono);
                medico.setObservaciones(observaciones);
                
                medicos.add(medico);
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectar();
        return medicos;
    }
     @Override   
    public void create(Citas cita){
        try{
          conectar();
          this.st = this.conexion.createStatement();
             
        //int id                  = cita.getId();
        Date dia                = cita.getDia();
        String hora             = cita.getHora();
        int id_paciente         = cita.getPacientes().getId();
        int id_medico           = cita.getMedicos().getId();
        String observaciones    = cita.getObservaciones();
        
        String sql = "insert into citas(dia,hora,id_paciente,id_medico,observaciones) values('" + sdf.format(dia) + "','" + hora + "','" + id_paciente + "','" + id_medico + "','" + observaciones + "')";

        System.out.println(sql);
        int salida = this.st.executeUpdate(sql);

        sql = "select max(id) as idmayor from citas;";
        System.out.println(sql);
        ResultSet resultado = this.st.executeQuery(sql);
        if (resultado.next()) {
            this.id = resultado.getInt("idmayor");
        }
        
        cita.setId(this.id);
        desconectar();
          
        }catch (SQLException se){
          se.printStackTrace();
        }
      }
    @Override
    public void update(Citas cita) {
        try {
            conectar();
            st = this.conexion.createStatement();
           
            String sql = "UPDATE citas "
            + "  SET dia='"         + sdf.format(cita.getDia())
            + "',hora='"            + cita.getHora()
            + "',id_paciente="      + cita.getPacientes().getId()
            + ",id_medico="         + cita.getMedicos().getId()
            + ",observaciones='"   + cita.getObservaciones()
            + "' WHERE id="         + cita.getId()
            + ";";
            
            System.out.println(sql);
            st.executeUpdate(sql);
            //Paciente paciente = getPaciente(cita.getPaciente().getId());
            //String nombre = paciente.getNombre();
            desconectar();
            vt.info("La cita del paciente" + cita.getPacientes().getNombre() + ", se ha actualizado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }
    @Override
    public void delete(Citas cita) {
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "DELETE FROM citas  "
                +  " WHERE id=" + cita.getId() + ";";
            System.out.println(sql);
            st.executeUpdate(sql);
            
            desconectar();
            vt.info("La Cita del paciente " + cita.getPacientes().getNombre() + ", se ha borrado correctamente");
            
        } catch (SQLException se) {
            se.printStackTrace();
            vt.error("La cita no se ha podido borrar.\n"
                    + "Error: " + se.getMessage());
        }
    }
    
    @Override
    public ArrayList readc() {
        
        ArrayList citas = new ArrayList();
        try {
            conectar();
            st = conexion.createStatement();
            String sql = "SELECT citas.*, medicos.id as id_medico, pacientes.id as id_paciente \n"
                       + "FROM `citas`, `medicos`, `pacientes`\n "
                       + "WHERE citas.id_paciente = pacientes.id and citas.id_medico = medicos.id\n"
                       + "ORDER BY citas.id;";
            
            System.out.println(sql);
            ResultSet resultado = st.executeQuery(sql);

            while (resultado.next()) {
                Citas cita = new Citas();
                Pacientes paciente = new Pacientes();
                Medicos medico = new Medicos();
        
                int id               = resultado.getInt("id");
                Date dia             = resultado.getDate("dia");
                String hora          = resultado.getString("hora");
                int id_paciente      = resultado.getInt("id_paciente");
                int id_medico        = resultado.getInt("id_medico");
                String observaciones = resultado.getString("observaciones");
                

                medico.setId(id_medico);
                paciente.setId(id_paciente);
                            
                
                cita.setId(id);
                cita.setDia(dia);
                cita.setHora(hora);
                cita.setObservaciones(observaciones);
                cita.setMedicos(medico);
                cita.setPacientes(paciente);
                
               citas.add(cita);
                
        
        }
             } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectar();
        return citas;
    }
    
    public Pacientes getPaciente(Pacientes paciente) {

        try {
           conectar();
            st = (Statement) conexion.createStatement();
            String sql = "SELECT * \n"
                + "FROM pacientes\n"
                + "WHERE id=" + paciente.getId() + ";";
            System.out.println(sql);
            ResultSet resultado = st.executeQuery(sql);

            while (resultado.next()) {
                paciente = new Pacientes();
        
                id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String dni = resultado.getString("dni");
                String nss = resultado.getString("nss");
                int edad = resultado.getInt("edad");
                int telefono = resultado.getInt("telefono");
                String observaciones = resultado.getString("observaciones");
                
                paciente.setId(id);
                paciente.setNombre(nombre);
                paciente.setDni(dni);
                paciente.setNss(nss);
                paciente.setEdad(edad);
                paciente.setTelefono(telefono);
                paciente.setObservaciones(observaciones);
             }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectar();
        
        return paciente;
    }
     
    
    public Medicos getMedico(Medicos medico) {
        
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "SELECT * \n"
                + "FROM medicos \n"
                + "WHERE id=" + medico.getId() + ";";
            System.out.println(sql);
            ResultSet resultado = st.executeQuery(sql);

            while (resultado.next()) {
                medico = new Medicos();
        
                id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String dni = resultado.getString("dni");
                String n_colegiado = resultado.getString("n_colegiado");
                String especialidad = resultado.getString("especialidad");
                int edad = resultado.getInt("edad");
                int telefono = resultado.getInt("telefono");
                String observaciones = resultado.getString("observaciones");
                
                medico.setId(id);
                medico.setNombre(nombre);
                medico.setDni(dni);
                medico.setNColegiado(n_colegiado);
                medico.setEspecialidad(especialidad);
                medico.setEdad(edad);
                medico.setTelefono(telefono);
                medico.setObservaciones(observaciones);
             }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectar();
        
        return medico;
        

    }
    public Citas getCita(Citas cita) {
        
        try {
            conectar();
            st = (Statement) conexion.createStatement();
            String sql = "SELECT * \n"
                + "FROM citas \n"
                + "WHERE id=" + cita.getId() + ";";
            System.out.println(sql);
            ResultSet resultado = st.executeQuery(sql);

            while (resultado.next()) {
                cita = new Citas();
                Medicos medico = new Medicos();
                Pacientes paciente = new Pacientes();
                
        
                int id               = resultado.getInt("id");
                Date dia             = resultado.getDate("dia");
                String hora          = resultado.getString("hora");
                int id_paciente      = resultado.getInt("id_paciente");
                int id_medico        = resultado.getInt("id_medico");
                String observaciones = resultado.getString("observaciones");
                

               
                            
                
                medico.setId(id_medico);
                paciente.setId(id_paciente);
                            
                
                cita.setId(id);
                cita.setDia(dia);
                cita.setHora(hora);
                cita.setObservaciones(observaciones);
                cita.setMedicos(medico);
                cita.setPacientes(paciente);
               
             }
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        desconectar();
        
        return cita;
        

    }

}
