/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro10.vista;

import java.awt.AWTKeyStroke;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**

* Fichero: VistaGraficaCitasMedico.java

* @author Darío Navarro Andrés <minombredario@gmail.com>

* @date 30-abr-2016

*/
public class VistaGraficaCitasMedico /*extends JFrame/*extends JInternalFrame*/{
    
    private JButton botonCitas,botonMedicos,botonSalir,botonLimpiar;
    private JPanel panelCitas, panelMedicos;
    private JScrollPane jScrollCitas, jScrollMedicos;
    private JTable TableCitas,TableMedicos;
    private JInternalFrame ventana;
    private JLabel datosPaciente1, datosPaciente2,datosPaciente3,datosPaciente4,datosPaciente5,datosPaciente6 ;
   
    public VistaGraficaCitasMedico(){
        ventana = new JInternalFrame();
        panelCitas = new JPanel();
        panelCitas.setBorder(BorderFactory.createTitledBorder("Citas"));
        jScrollCitas = new JScrollPane();
        TableCitas = new JTable();
        botonCitas = new JButton("Citas");
        panelMedicos = new JPanel();
        panelMedicos.setBorder(BorderFactory.createTitledBorder("Medicos"));
        jScrollMedicos = new JScrollPane();
        TableMedicos = new JTable();
        botonMedicos = new JButton("Medicos");
        botonSalir = new JButton("SALIR");
        botonLimpiar = new JButton("Refresh");
        datosPaciente1 = new JLabel(" ");
        datosPaciente2 = new JLabel(" ");
        datosPaciente3 = new JLabel(" ");
        datosPaciente4 = new JLabel(" ");
        datosPaciente5 = new JLabel(" ");
        datosPaciente6 = new JLabel(" ");
       
ventana.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TableCitas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollCitas.setViewportView(TableCitas);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(panelCitas);
        panelCitas.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollCitas, javax.swing.GroupLayout.PREFERRED_SIZE, 593, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonCitas)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(datosPaciente2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(datosPaciente3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(datosPaciente1, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(datosPaciente4, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(datosPaciente5, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(datosPaciente6,javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(botonCitas))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollCitas, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(datosPaciente1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(datosPaciente2)
                    .addComponent(datosPaciente3)
                    .addComponent(datosPaciente4)
                    .addComponent(datosPaciente5)
                    .addComponent(datosPaciente6))
                .addGap(537, 537, 537))
        );

        TableMedicos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollMedicos.setViewportView(TableMedicos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(panelMedicos);
        panelMedicos.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, 592, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonMedicos)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonMedicos)
                .addGap(59, 59, 59))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(ventana.getContentPane());
        ventana.getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelMedicos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(botonLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonSalir)
                .addGap(20, 20, 20))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCitas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonSalir)
                    .addComponent(botonLimpiar))
                .addGap(32, 32, 32)
                .addComponent(panelCitas, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(panelMedicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
        );

        ventana.pack();
        ventana.setVisible(true);
    }

    public JButton getBotonCitas() {
        return botonCitas;
    }

    public JButton getBotonMedicos() {
        return botonMedicos;
    }

    public JButton getBotonSalir() {
        return botonSalir;
    }

    public JButton getBotonLimpiar() {
        return botonLimpiar;
    }

    public JPanel getPanelCitas() {
        return panelCitas;
    }

    public JPanel getPanelMedicos() {
        return panelMedicos;
    }

    public JScrollPane getjScrollCitas() {
        return jScrollCitas;
    }

    public JScrollPane getjScrollMedicos() {
        return jScrollMedicos;
    }

    public JTable getTableCitas() {
        return TableCitas;
    }

    public JTable getTableMedicos() {
        return TableMedicos;
    }

    public JInternalFrame getVentana() {
        return ventana;
    }
    
   public JLabel getDatosPaciente1() {
        return datosPaciente1;
    }

    public JLabel getDatosPaciente2() {
        return datosPaciente2;
    }

    public JLabel getDatosPaciente3() {
        return datosPaciente3;
    }

    public JLabel getDatosPaciente4() {
        return datosPaciente4;
    }

    public JLabel getDatosPaciente5() {
        return datosPaciente5;
    }

    public JLabel getDatosPaciente6() {
        return datosPaciente6;
    }
    
    /*public static void main(String[] args) {
        new VistaGraficaCitasMedico();
    }*/

    
}