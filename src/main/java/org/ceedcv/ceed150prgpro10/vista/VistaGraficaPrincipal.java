/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.vista;

import java.awt.*;
import javax.swing.*;



/**
 *
 * @author usu1601
 */
public class VistaGraficaPrincipal extends JFrame{
    private JMenu MenuPaciente, MenuMedico, MenuCita, MenuHerramientas, MenuDocumentos, MenuSalir;
    private JMenuItem ItemTC, ItemTM, ItemTP, ItemInstalar, ItemEliminar, ItemDatos, ItemTablas, ItemDb4o, ItemModelo, ItemAcerca, ItemPdf, ItemWeb, ItemCita, ItemPaciente, ItemMedico, ItemSalir, ItemCitas;
    private JMenuBar BarraMenu;
    private JPanel panel;
    private JFrame contenedor;
    private JDesktopPane escritorio;
    
      
    public VistaGraficaPrincipal(){
    
        contenedor = new JFrame();
        
        panel = new JPanel();
        escritorio = new JDesktopPane();
        panel.setLayout(null);
        contenedor.setContentPane(panel);
        contenedor.setContentPane(escritorio);
        
 
        //Creamos la barra de Menu
        BarraMenu = new JMenuBar();
 
        //Creamos los menus
        MenuPaciente = new JMenu("Pacientes");
            ItemPaciente = new JMenuItem("Paciente");
            ItemTP = new JMenuItem("Tabla Pacientes");
        MenuMedico = new JMenu("Medicos");
            ItemMedico = new JMenuItem("Medico");
            ItemTM = new JMenuItem("Tabla Medicos");
	MenuCita = new JMenu("Citas");
            ItemCita = new JMenuItem("Cita");
            ItemCitas = new JMenuItem("Relacion Citas");
            ItemTC = new JMenuItem("Tabla Citas");
	MenuHerramientas = new JMenu("Herramientas");
            ItemInstalar = new JMenuItem("Instalar Base de Datos");
            ItemDb4o = new JMenuItem("Instalar Base de Datos");
            ItemEliminar = new JMenuItem("Desinstalar Base de Datos");
            ItemDatos = new JMenuItem("Datos Ejemplo");
            ItemTablas =new JMenuItem("Crear tablas");
            ItemAcerca =new JMenuItem("Acerca");
            ItemModelo =new JMenuItem("Modelos");
	MenuDocumentos = new JMenu("Documentos");
            ItemPdf = new JMenuItem("Pdf");
            ItemWeb = new JMenuItem("WEB");
        MenuSalir = new JMenu("Salir");
            ItemSalir= new JMenuItem("Salir");
        
        //Añadimos los menus a la barra de menu
        BarraMenu.add(MenuPaciente);
            MenuPaciente.add(ItemPaciente);
            MenuPaciente.add(ItemTP);
        BarraMenu.add(MenuMedico);
            MenuMedico.add(ItemMedico);
            MenuMedico.add(ItemTM);
        BarraMenu.add(MenuCita);
            MenuCita.add(ItemCita);
            MenuCita.add(ItemCitas);
            MenuCita.add(ItemTC);
        BarraMenu.add(MenuHerramientas);
            MenuHerramientas.add(ItemInstalar);
            MenuHerramientas.add(ItemDb4o);
            MenuHerramientas.add(ItemEliminar);
            MenuHerramientas.add(ItemTablas);
            MenuHerramientas.add(ItemDatos);
            MenuHerramientas.add(ItemModelo);
            MenuHerramientas.add(ItemAcerca);   
        BarraMenu.add(MenuDocumentos);
            MenuDocumentos.add(ItemPdf);
            MenuDocumentos.add(ItemWeb);
        BarraMenu.add(MenuSalir);
            MenuSalir.add(ItemSalir);
      /*VistaGraficaMenu vista = new VistaGraficaMenu();  
      escritorio.add(vista.ventana);      */ 
        //Indicamos que es el menu por defecto
        contenedor.setJMenuBar(BarraMenu);
        //contenedor.setVisible(true);
        
        //ventana.setLocationRelativeTo(null);
            contenedor.setSize(800,500);
        //este metodo devuelve el tamaño de la pantalla
            Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        //obtenemos el tamaño de la ventana
            Dimension window = contenedor.getSize();
        //para centrar la ventana lo hacemos con el siguiente calculo
        contenedor.setLocation((pantalla.width-window.width)/2, (pantalla.height-window.height)/2);
	contenedor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
    }
    

    public JMenuItem getItemInstalar() {
        return ItemInstalar;
    }

    /*
    public static void main(String[] args) {
    VistaGraficaPrincipal principal = new VistaGraficaPrincipal();
    }*/
    public JMenuItem getItemTablas() {
        return ItemTablas;
    }

    public JMenu getMenuPaciente() {
        return MenuPaciente;
    }

    public JMenu getMenuMedico() {
        return MenuMedico;
    }

    public JMenu getMenuCita() {
        return MenuCita;
    }

    public JMenuItem getItemCitas() {
        return ItemCitas;
    }

    public JMenuItem getItemTC() {
        return ItemTC;
    }

    public JMenuItem getItemTM() {
        return ItemTM;
    }

    public JMenuItem getItemTP() {
        return ItemTP;
    }

    public JMenu getMenuHerramientas() {
        return MenuHerramientas;
    }

    public JMenu getMenuDocumentos() {
        return MenuDocumentos;
    }

    public JMenu getMenuSalir() {
        return MenuSalir;
    }

    public JMenuItem getItemEliminar() {
        return ItemEliminar;
    }
    
    public JMenuItem getItemAcerca() {
        return ItemAcerca;
    }

    public JMenuItem getItemModelo() {
        return ItemModelo;
    }

    public JMenuItem getItemPdf() {
        return ItemPdf;
    }

    public JMenuItem getItemWeb() {
        return ItemWeb;
    }

    public JMenuItem getItemCita() {
        return ItemCita;
    }

    public JMenuItem getItemPaciente() {
        return ItemPaciente;
    }

    public JMenuItem getItemMedico() {
        return ItemMedico;
    }

    public JMenuItem getItemSalir() {
        return ItemSalir;
    }

    public JMenuBar getBarraMenu() {
        return BarraMenu;
    }

    public JPanel getPanel() {
        return panel;
    }

    public JFrame getContenedor() {
        return contenedor;
    }

    public JDesktopPane getEscritorio() {
        return escritorio;
    }

    public JMenuItem getItemDatos() {
        return ItemDatos;
    }

    public JMenuItem getItemDb4o() {
        return ItemDb4o;
    }

  }