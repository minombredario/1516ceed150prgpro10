/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ceedcv.ceed150prgpro10.controlador;

import java.io.IOException;
import javax.swing.JOptionPane;
import org.ceedcv.ceed150prgpro10.modelo.IModelo;
import org.ceedcv.ceed150prgpro10.modelo.ModeloDb4o;
import org.ceedcv.ceed150prgpro10.modelo.ModeloFichero;
import org.ceedcv.ceed150prgpro10.modelo.ModeloHibernate;
import org.ceedcv.ceed150prgpro10.modelo.ModeloMysql;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaPrincipal;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;

/**
 *
 * @author usu1601
 */
public class ControladorEleccion {
    private VistaTerminal vt;
    private IModelo modelo;
    private Controlador controlador;
    VistaGraficaPrincipal vistagrafica;
    public  ControladorEleccion() throws IOException   {   
        
        vistagrafica = new VistaGraficaPrincipal();
                
		String[] modelos = {"CSV", "MYSQL", "DB4O", "HIBERNATE"};
                int respuesta = JOptionPane.showOptionDialog(null, "Escoge sistema para guardar los datos", "Escoger sistema",
				0, 0, null, modelos, null);
		switch (respuesta) {
			case 0:
                            this.modelo = new ModeloFichero();
                            vistagrafica.getContenedor().setTitle("Tema 6. Ficheros");
                            vistagrafica.getItemEliminar().setVisible(false);
                            vistagrafica.getItemInstalar().setVisible(false);
                            vistagrafica.getItemDb4o().setVisible(false);
                            vistagrafica.getItemTablas().setVisible(false);
                            vistagrafica.getItemDatos().setVisible(false);
                            break;
			case 1:
                            this.modelo = new ModeloMysql();
                            vistagrafica.getContenedor().setTitle("Tema 8. Mysql");
                            vistagrafica.getItemEliminar().setVisible(true);
                            vistagrafica.getItemInstalar().setVisible(true);
                            vistagrafica.getItemDb4o().setVisible(false);
                            vistagrafica.getItemTablas().setVisible(true);
                            vistagrafica.getItemDatos().setVisible(true);
                            break;
			case 2:
                            this.modelo = new ModeloDb4o();
                            vistagrafica.getContenedor().setTitle("Tema 7. Db4o");
                            vistagrafica.getItemEliminar().setVisible(true);
                            vistagrafica.getItemInstalar().setVisible(false);
                            vistagrafica.getItemDb4o().setVisible(true);
                            vistagrafica.getItemTablas().setVisible(false);
                            vistagrafica.getItemDatos().setVisible(false);
                            break;
			case 3:
                            this.modelo = new ModeloHibernate();
                            vistagrafica.getContenedor().setTitle("Tema 10. Hibernate");
                            vistagrafica.getItemEliminar().setVisible(true);
                            vistagrafica.getItemInstalar().setVisible(true);
                            vistagrafica.getItemDb4o().setVisible(false);
                            vistagrafica.getItemTablas().setVisible(true);
                            vistagrafica.getItemDatos().setVisible(true);
                            break;
                
		}
             vistagrafica.getContenedor().setVisible(true);
             controlador = new Controlador(modelo, vistagrafica);
    }
    
}
   
   
