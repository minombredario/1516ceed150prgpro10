package org.ceedcv.ceed150prgpro10.controlador;

import org.ceedcv.ceed150prgpro10.vista.VistaGraficaPaciente;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaCita;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaAcerca;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaMenu;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaPrincipal;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaMedico;
import org.ceedcv.ceed150prgpro10.vista.VistaTerminal;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import org.ceedcv.ceed150prgpro10.modelo.IModelo;
import org.ceedcv.ceed150prgpro10.vista.VistaTablaCitas;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaCitasMedico;
import org.ceedcv.ceed150prgpro10.vista.VistaTablaMedicos;
import org.ceedcv.ceed150prgpro10.vista.VistaTablaPacientes;




/**
 * Fichero: ControladorCita.java
 * @author Darío Navarro Andrés <minombredario@gmail.com>
 * @Date 12-dic-2015
 */
public class Controlador implements ActionListener {
  
    private VistaTerminal vt = new VistaTerminal();
    private IModelo modelo;
    
    private VistaGraficaPrincipal vistagrafica = null;
    private VistaGraficaMenu menugrafica = null;
    private VistaGraficaPaciente vgp = null;
    private VistaGraficaMedico vgm = null;
    private VistaGraficaCita vgc = null;
    private VistaGraficaAcerca vga = null;
    private VistaTablaCitas vtc = null;
    private VistaTablaMedicos vtm = null;
    private VistaTablaPacientes vtp = null;
    private ControladorEleccion ce;
    private JInternalFrame jiframe;
    
   public Controlador(){
       
   } 
   public Controlador(IModelo modelo, VistaGraficaPrincipal vistagrafica) throws IOException {
       
        this.vistagrafica = vistagrafica;
        this.modelo = modelo;     
        
        
        Portada();
        
        this.menugrafica.getBotonMedico().addActionListener(this);
        this.menugrafica.getBotonPaciente().addActionListener(this);
        this.menugrafica.getBotonCita().addActionListener(this);
        this.menugrafica.getBotonDocumentacion().addActionListener(this);
        this.menugrafica.getBotonAcerca().addActionListener(this);
        this.menugrafica.getBotonSalir().addActionListener(this);
        //items menus Paciente, Medico, Cita
        this.vistagrafica.getItemPaciente().addActionListener(this);
        this.vistagrafica.getItemTP().addActionListener(this);
        this.vistagrafica.getItemMedico().addActionListener(this);
        this.vistagrafica.getItemTM().addActionListener(this);
        this.vistagrafica.getItemCita().addActionListener(this);
        this.vistagrafica.getItemCitas().addActionListener(this);
        this.vistagrafica.getItemTC().addActionListener(this);
        //itmes menu Herramientas
        this.vistagrafica.getItemAcerca().addActionListener(this);
        this.vistagrafica.getItemEliminar().addActionListener(this);
        this.vistagrafica.getItemInstalar().addActionListener(this);
        this.vistagrafica.getItemDb4o().addActionListener(this);
        this.vistagrafica.getItemTablas().addActionListener(this);
        this.vistagrafica.getItemDatos().addActionListener(this);
        this.vistagrafica.getItemModelo().addActionListener(this);
         //items menu Documentos
        this.vistagrafica.getItemPdf().addActionListener(this);
        this.vistagrafica.getItemWeb().addActionListener(this);
        //Item menu Salir
        this.vistagrafica.getItemSalir().addActionListener(this);
        
       
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object evento = e.getSource();
        
        if(menugrafica.getBotonPaciente() == evento|| vistagrafica.getItemPaciente() == evento){
           VistaPaciente();
        }else if(menugrafica.getBotonMedico() == evento || vistagrafica.getItemMedico() == evento){
            VistaMedico();
        }else if(menugrafica.getBotonCita() == evento || vistagrafica.getItemCita() == evento){
            VistaCita();
        }else if(vistagrafica.getItemCitas() == evento){
            VistaCitas();
        }else if(vistagrafica.getItemTC() == evento){
            tablaCitas();
        }else if(vistagrafica.getItemTM() == evento){
            tablaMedicos();
        }else if(vistagrafica.getItemTP() == evento){
            tablaPacientes();
            
    //herramientas  
        }else if(vistagrafica.getItemDb4o() == evento){
            InstalarDb4o();
        }else if(vistagrafica.getItemEliminar() == evento){
           Desinstalar();           
        }else if (vistagrafica.getItemInstalar() == evento){
            InstalarBD();
            vt.mostrarTexto("SQL");
        }else if (vistagrafica.getItemTablas() == evento){
            CrearTablas();
        }else if (vistagrafica.getItemDatos() == evento){
            CrearDatos();
        }else if(menugrafica.getBotonAcerca() == evento || vistagrafica.getItemAcerca() == evento){
           VistaAcerca();
        }else if(vistagrafica.getItemModelo() == evento){
            
            try {
                ce = new ControladorEleccion();
            } catch (IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else if(vistagrafica.getItemPdf() == evento){
            String pdf= "src/main/java/org/ceedcv/ceed150prgpro8/documentacion/documentacion.pdf";
            try {
                File path = new File (pdf);
                Desktop.getDesktop().open(path);
            }catch (IOException ex) {
                ex.printStackTrace();
            }
            
        }else if( menugrafica.getBotonDocumentacion() == evento || vistagrafica.getItemWeb()== evento){
            String url = "https://docs.google.com/document/d/1fZhIldQ0L7OKNU0T9swRUIE1Qri9usMAVm__Qz_5gpM/edit?ts=561d4e27";
            
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.BROWSE)) {
                try {
                    desktop.getDesktop().browse(new URI(url));
                } catch (URISyntaxException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }else if(menugrafica.getBotonSalir() == evento || vistagrafica.getItemSalir() == evento){
           Object [] opciones ={"Aceptar","Cancelar"};
            Component rootPane = null;
            int eleccion = JOptionPane.showOptionDialog(rootPane,"¿Desea cerrar la aplicacion?","Mensaje de Confirmacion",
            JOptionPane.YES_NO_OPTION,
            JOptionPane.QUESTION_MESSAGE,null,opciones,"Aceptar");
                if (eleccion == JOptionPane.YES_OPTION){
            
                    System.exit(0);
                }else{ }
        }
    }
    
    public void Portada(){
        
        menugrafica = new VistaGraficaMenu();  
        
       
        //menugrafica.getVentana().setResizable(false);       
        menugrafica.getVentana().setMaximizable(true);
        menugrafica.getVentana().setIconifiable(true);
        //menugrafica.getVentana().setClosable(true);
            try{
                vistagrafica.getEscritorio().add(menugrafica.getVentana());
                menugrafica.getVentana().setVisible(true);
                this.menugrafica.getVentana().setMaximum(true);
            }catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public void VistaPaciente(){
        vgp = new VistaGraficaPaciente();
                        
        vistagrafica.getEscritorio().add(vgp.getVentana()); 
        vgp.getVentana().setVisible(true);
        //vgp.getVentana().setResizable(false);       
        vgp.getVentana().setMaximizable(true);
        vgp.getVentana().setIconifiable(true);
        //vgp.getVentana().setClosable(true);
            try {
                ControladorPaciente cp = new ControladorPaciente(vgp, modelo);
                vgp.getVentana().setMaximum(true);
                vgp.getVentana().setSelected(true);
                } catch (PropertyVetoException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                } 
       
    }
    
    public void VistaMedico(){
        
        vgm = new VistaGraficaMedico();
        
        vistagrafica.getEscritorio().add(vgm.getVentana()); 
        vgm.getVentana().setVisible(true);
        //vgm.getVentana().setResizable(false);       
        vgm.getVentana().setMaximizable(true);
        vgm.getVentana().setIconifiable(true);
        //vgm.getVentana().setClosable(true);
            try {
                ControladorMedico cm = new ControladorMedico(vgm, modelo);
                vgm.getVentana().setMaximum(true);
                vgm.getVentana().setSelected(true);
                }catch(PropertyVetoException ex){
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }catch(IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public void VistaCita(){
       
        vgc = new VistaGraficaCita();
            
        vistagrafica.getEscritorio().add(vgc.getVentana());
        vgc.getVentana().setVisible(true);
        //vgc.getVentana().setResizable(false);
        vgc.getVentana().setMaximizable(true);
        vgc.getVentana().setIconifiable(true);
        //vgc.getVentana().setClosable(true);
        try {
            ControladorCita cc = new ControladorCita(vgc, modelo);
            vgc.getVentana().setMaximum(true);
            vgc.getVentana().setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void VistaAcerca(){
        
        vga = new VistaGraficaAcerca();  
                
        vistagrafica.getEscritorio().add(vga.getVentana()); 
        vga.getVentana().setVisible(true);
        //vga.getVentana().setResizable(false);       
        vga.getVentana().setMaximizable(true);
        vga.getVentana().setIconifiable(true);
        //vga.getVentana().setClosable(true);
            try {
                ControladorAcerca ca = new ControladorAcerca(vga, modelo);
                vga.getVentana().setMaximum(true);
                vga.getVentana().setSelected(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    private void InstalarDb4o() {
        try {
            modelo.instalar();
            vt.info("Base de datos creada con exito");
            
        } catch (Exception e) {
            e.printStackTrace();
            vt.error("Ha ocurrido un error durante la instalación");
        }
    }

    private void Desinstalar() {
        try{
            modelo.desinstalar();
            vt.info("Base de datos eliminada con exito");
        }catch (Exception e){
            e.printStackTrace();
            vt.error("Ha ocurrido un error durante la desinstalación");
        }
    }
    
    private void InstalarBD() {
        
        try {
            modelo.instalar();
            vt.info("Base de datos creada con exito");
            
        } catch (Exception e) {
            e.printStackTrace();
            vt.error("Ha ocurrido un error durante la instalación");
        }
    }
    
    private void CrearTablas() {
        
        try{
            modelo.CrearTablas();
            vt.info("Tablas creadas con exito");
        }catch (Exception e){
            vt.error("Ha ocurrido un error durante la creacion de las tablas");
        }
    }
    private void CrearDatos() {
        try{
            modelo.CrearDatos();
            vt.info("Datos creados con exito");
        }catch (Exception e){
            vt.error("Ha ocurrido un error durante la creacion de las tablas");
        }
    }
    

    private void VistaCitas() {
        VistaGraficaCitasMedico vgcs = new VistaGraficaCitasMedico();
            
        vistagrafica.getEscritorio().add(vgcs.getVentana());
        
        try {
            vgcs.getVentana().setVisible(true);
            //vgcs.getVentana().setResizable(false);
            vgcs.getVentana().setMaximizable(true);
            vgcs.getVentana().setIconifiable(true);
            //vgcs.getVentana().setClosable(true);
            vgcs.getVentana().setSelected(true);
            vgcs.getVentana().setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ControladorCitasMedicos ccp = new ControladorCitasMedicos(vistagrafica,vgcs,modelo);
    }
    
    private void tablaCitas() {
        jiframe = new JInternalFrame();
        VistaTablaCitas jpanel = new VistaTablaCitas();
        jiframe.add(jpanel);
        vistagrafica.getEscritorio().add(jiframe);
        jiframe.setVisible(true);
        try {
            jiframe.setMaximum(true);
            jiframe.setSelected(true);
        } catch (PropertyVetoException localPropertyVetoException) {
        }
    }

    private void tablaMedicos() {
        jiframe = new JInternalFrame();
        VistaTablaMedicos jpanel = new VistaTablaMedicos();
        jiframe.add(jpanel);
        vistagrafica.getEscritorio().add(jiframe);
        jiframe.setVisible(true);
        try {
            jiframe.setMaximum(true);
            jiframe.setSelected(true);
        } catch (PropertyVetoException localPropertyVetoException) {
        }
    }

    private void tablaPacientes() {
        jiframe = new JInternalFrame();
        VistaTablaPacientes jpanel = new VistaTablaPacientes();
        jiframe.add(jpanel);
        vistagrafica.getEscritorio().add(jiframe);
        jiframe.setVisible(true);
        try {
            jiframe.setMaximum(true);
            jiframe.setSelected(true);
        } catch (PropertyVetoException localPropertyVetoException) {
        }
    }

    public JInternalFrame getJiframe() {
        return jiframe;
    }
    
}