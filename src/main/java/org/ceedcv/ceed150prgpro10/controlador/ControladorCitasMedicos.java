/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.ceedcv.ceed150prgpro10.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import org.ceedcv.ceed150prgpro10.modelo.Citas;
import org.ceedcv.ceed150prgpro10.modelo.IModelo;
import org.ceedcv.ceed150prgpro10.modelo.Medicos;
import org.ceedcv.ceed150prgpro10.modelo.Pacientes;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaCita;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaCitasMedico;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaMedico;
import org.ceedcv.ceed150prgpro10.vista.VistaGraficaPrincipal;


/**

* Fichero: ControladorCitasMedicos.java

* @author Darío Navarro Andrés <minombredario@gmail.com>

* @date 30-abr-2016

*/
public class ControladorCitasMedicos implements ActionListener, MouseListener{

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private VistaGraficaCitasMedico vgcs;
    private IModelo modelo;
    private ArrayList citas, pacientes,medicos;
    private VistaGraficaPrincipal vg = null;
    
    public ControladorCitasMedicos(VistaGraficaPrincipal vg,VistaGraficaCitasMedico vgcs, IModelo modelo) {
        this.vgcs = vgcs;
        this.modelo = modelo;
        this.vg = vg;
        
        this.vgcs.getBotonCitas().addActionListener(this);
        this.vgcs.getBotonMedicos().addActionListener(this);
        this.vgcs.getBotonSalir().addActionListener(this);
        this.vgcs.getBotonLimpiar().addActionListener(this);
        this.vgcs.getTableMedicos().addMouseListener(this);
        this.vgcs.getTableCitas().addMouseListener(this);
        DatosTablas();
    }
        @Override
    public void actionPerformed(ActionEvent e) {
        
        Object evento = e.getSource();
        if (this.vgcs.getBotonCitas() == evento){
            VistaCita();
        }else if (this.vgcs.getBotonMedicos() == evento){
           VistaMedico();
        }else if (this.vgcs.getBotonSalir() == evento){
            this.vgcs.getVentana().dispose();
        }else if (this.vgcs.getBotonLimpiar() == evento){
            DatosTablas();
        }   
    }
    
    private void DatosTablas() {
        pacientes = modelo.readp();
        citas = modelo.readc();
        medicos = modelo.readm();
        //tablePacientes(pacientes);
        tableMedicos(medicos);
        tableCitas(citas);
        /*if (pacientes != null){
            Paciente pa = (Paciente)pacientes.get(0);
            relCiPa(citas,pa);
        }*/
         if (medicos != null){
            Medicos me = (Medicos)medicos.get(0);
        }
        
        if(citas != null){
            Citas ci = (Citas)citas.get(0);
        }
        vaciarLabels();
    }
    private void tableCitas(ArrayList citas) {
        if (!citas.isEmpty()) {
            vgcs.getTableCitas().setVisible(true);
            String[] cabecera_tabla = { "Id", "Fecha", "Hora", "Observaciones", "Paciente"};
      
            String[] tablas = new String[cabecera_tabla.length];
            DefaultTableModel Tmodelo = new DefaultTableModel((Object[][])null, cabecera_tabla);
            
            
            Iterator it = citas.iterator();
            while (it.hasNext()) {
                Citas cita = (Citas)it.next();
                Pacientes paciente = getPaciente(cita.getPacientes().getId());
                
                tablas[0] = Integer.toString(cita.getId());
                tablas[1] = sdf.format(cita.getDia());
                tablas[2] = cita.getHora();
                tablas[3] = cita.getObservaciones();
                tablas[4] = Integer.toString(cita.getPacientes().getId()) + ": " + paciente.getNombre()/*cita.getPaciente().getNombre()*/;
                //tablas[5] = Integer.toString(cita.getMedico().getId());
                
                Tmodelo.addRow(tablas);
            }
            vgcs.getTableCitas().setModel(Tmodelo);
        }else {
            vgcs.getTableCitas().setVisible(false);
        }
    }
    private void tableMedicos(ArrayList medicos) {
        if (!medicos.isEmpty()) {
            vgcs.getTableMedicos().setVisible(true);
            String[] cabecera_tabla = { "Id", "Nombre", "Edad", "Dni", "Especialidad","NºColegiado", "Telefono", "Observaciones"};
      
            String[] tablas = new String[cabecera_tabla.length];
            DefaultTableModel Tmodelo = new DefaultTableModel((Object[][])null, cabecera_tabla);
      
            Iterator it = medicos.iterator();
            while (it.hasNext()) {
                Medicos me = (Medicos)it.next();
               
                    tablas[0] = Integer.toString(me.getId());
                    tablas[1] = me.getNombre();
                    tablas[2] = Integer.toString(me.getEdad());
                    tablas[3] = me.getDni();
                    tablas[4] = me.getNColegiado();
                    tablas[5] = me.getEspecialidad();
                    tablas[6] = Integer.toString(me.getTelefono());
                    tablas[7] = me.getObservaciones();
                    Tmodelo.addRow(tablas);
            }
        
        vgcs.getTableMedicos().setModel(Tmodelo);
        }else {
            vgcs.getTableMedicos().setVisible(false);
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {
        Object objeto = e.getSource();
        if (objeto == vgcs.getTableMedicos()) {
            pulsado(e);
        }
        else if (objeto == vgcs.getTableCitas()){
            paciente(e);
        }else {vaciarLabels();}
    }
    private void pulsado(MouseEvent e){
     Medicos medico = null;
        citas = modelo.readc();
        int row = vgcs.getTableMedicos().rowAtPoint(e.getPoint());
        if (row != -1) {
            String id = (String)vgcs.getTableMedicos().getValueAt(row, 0);
            int i = 0;
            while (i < medicos.size()) {
                medico = (Medicos)medicos.get(i);
                if (Integer.toString(medico.getId()).equals(id)) {
                    vaciarLabels();
                    break;
                }
                i++;
            }
        }
        if (medico != null) {    
            tablaMedicosCitas(citas, medico);
        }
    }
    private void paciente (MouseEvent e){
        Citas cita = null;
        citas = modelo.readc();
        int row = vgcs.getTableCitas().rowAtPoint(e.getPoint());
        if (row != -1) {
            String id = (String)vgcs.getTableCitas().getValueAt(row, 0);
            
            int i = 0;
            while (i < citas.size()) {
                cita = (Citas)citas.get(i);
                if (Integer.toString(cita.getId()).equals(id)) {
                int idp = cita.getPacientes().getId();
                Pacientes paciente = getPaciente(idp);
                    
                String nombre = paciente.getNombre();
                String dni = paciente.getDni();
                String nss = paciente.getNss();
                int edad = paciente.getEdad();
                int telefono = paciente.getTelefono();
                String observaciones = paciente.getObservaciones();
                
                vgcs.getDatosPaciente1().setText("Nombre: " + nombre);
                vgcs.getDatosPaciente2().setText("Dni: " + dni);
                vgcs.getDatosPaciente3().setText("Nss: " + nss);
                vgcs.getDatosPaciente4().setText("Edad: " + edad);
                vgcs.getDatosPaciente5().setText("Telefono: " + telefono);
                vgcs.getDatosPaciente6().setText("Observaciones: " + observaciones);
                break;
                }
                i++;
            } 
        } 
    }
    private Pacientes getPaciente(int id) {
         
        Pacientes paciente = null;
        
        Iterator iterator = modelo.readp().iterator();
        while (iterator.hasNext()) {                    
            Pacientes pa = (Pacientes) iterator.next();
            if (id == (pa.getId())) {                
                paciente = pa;
            }/*else paciente = null;*/
        } 
        return paciente;                                 
    }
    
    private void vaciarLabels(){
        vgcs.getDatosPaciente1().setText("");
        vgcs.getDatosPaciente2().setText("");
        vgcs.getDatosPaciente3().setText("");
        vgcs.getDatosPaciente4().setText("");
        vgcs.getDatosPaciente5().setText("");
        vgcs.getDatosPaciente6().setText("");
    }
    private void tablaMedicosCitas(ArrayList citas, Medicos medico) {
        if (!citas.isEmpty()) {
            
            vgcs.getTableCitas().setVisible(true);
            String[] cabecera_tabla = { "Id", "Fecha", "Hora", "Observaciones", "Paciente"};
      
            String[] tablas = new String[cabecera_tabla.length];
            DefaultTableModel Tmodelo = new DefaultTableModel((Object[][])null, cabecera_tabla);
           
      
            Iterator it = citas.iterator();
            while (it.hasNext()) {
                Citas cita = (Citas)it.next();
                    if(cita.getMedicos().getId() == medico.getId()){
                        tablas[0] = Integer.toString(cita.getId());
                        tablas[1] = sdf.format(cita.getDia());
                        tablas[2] = cita.getHora();
                        tablas[3] = cita.getObservaciones();
                        tablas[4] = Integer.toString(cita.getPacientes().getId()) + ": " + cita.getPacientes().getNombre();
                        //tablas[5] = Integer.toString(cita.getMedico().getId());
                
                        Tmodelo.addRow(tablas);
                    }
            }
            vgcs.getTableCitas().setModel(Tmodelo);
        }else{
            vgcs.getTableCitas().setVisible(false);
        }
    }
   
    public void VistaMedico(){
        VistaGraficaMedico vgm = new VistaGraficaMedico();
        
        vg.getEscritorio().add(vgm.getVentana()); 
        vgm.getVentana().setVisible(true);
        //vgm.getVentana().setResizable(false);       
        vgm.getVentana().setMaximizable(true);
        vgm.getVentana().setIconifiable(true);
        //vgm.getVentana().setClosable(true);
            try {
                ControladorMedico cm = new ControladorMedico(vgm, modelo);
                vgm.getVentana().setMaximum(true);
                vgm.getVentana().setSelected(true);
                }catch(PropertyVetoException ex){
                    Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }catch(IOException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public void VistaCita(){
       
        VistaGraficaCita vgc = new VistaGraficaCita();
            
        vg.getEscritorio().add(vgc.getVentana());
        vgc.getVentana().setVisible(true);
        //vgc.getVentana().setResizable(false);
        vgc.getVentana().setMaximizable(true);
        vgc.getVentana().setIconifiable(true);
        //vgc.getVentana().setClosable(true);
        try {
            ControladorCita cc = new ControladorCita(vgc, modelo);
            vgc.getVentana().setMaximum(true);
            vgc.getVentana().setSelected(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    @Override
    public void mousePressed(MouseEvent e) {
        //no usado
    }

    @Override
    public void mouseReleased(MouseEvent e) {
      //no usado
       
    }

    @Override
    public void mouseEntered(MouseEvent e) {
       //no usado 
    }
    

    @Override
    public void mouseExited(MouseEvent e) {
        //no usado
    }
}
    